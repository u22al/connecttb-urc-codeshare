/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by silvap on 1/9/15.
 */
public class SideEffectsListAdapter extends ArrayAdapter<JSONObject> {

    public SideEffectsListAdapter(Activity context, List<JSONObject> doses) {
        super(context, R.layout.view_side_effect_checkbox, doses);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        JSONObject sideEffect = getItem(position);

        View vi = convertView;

        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.view_side_effect_checkbox, parent, false);
        }

        String label = "???";
        boolean reported = false;
        try {
            label = sideEffect.getString(SurveyData.KEY_SCREENING_QUESTION);
            reported = sideEffect.getBoolean(SurveyData.KEY_REPORTED);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CheckBox doseCheckbox = (CheckBox) vi.findViewById(R.id.checkBox);
        doseCheckbox.setText(label);
        doseCheckbox.setChecked(reported);

        return vi;
    }

}
