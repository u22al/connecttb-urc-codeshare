/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.data;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.urc_chs.connectb.pm.utils.Settings;
import com.urc_chs.connectb.pm.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by silvap on 1/11/15.
 */
public class SurveyDatabase {

    public enum Status {
        IDLE,
        UPLOADING,
        AUTH_ERROR,
    }

    public static String TAG = Utils.TAG + "/SurveyData";

    private static String PREFS_KEY = "SurveyData";
    private static String DATA = "data";

    private static SurveyDatabase ourInstance = new SurveyDatabase();

    public static SurveyDatabase getInstance() {
        return ourInstance;
    }

    private volatile JSONArray mSurveys;
    private volatile Status mStatus = Status.IDLE;

    private volatile boolean mAuthError = false;

    private SurveyDatabase() {
        mSurveys = new JSONArray();
    }

    private void loadFromStorage(SharedPreferences preferences) {
        Log.d(TAG, "Loading from storage...");
        // SurveyData list
        String json = preferences.getString(DATA, "");
        try {
            mSurveys = new JSONArray(json);
        } catch (JSONException e) {
            Log.d(TAG, "Load from storage failed");
            return;
        }

        Log.d(TAG, "Loaded: SurveyData(" + mSurveys.length() + ")");
    }

    private void saveToStorage(SharedPreferences preferences) {
        Log.d(TAG, "Saving to storage...");
        SharedPreferences.Editor editor = preferences.edit();
        // SurveyData list
        editor.putString(DATA, mSurveys.toString());
        //
        editor.commit();
        Log.d(TAG, "Saving completed");
    }

    public synchronized static boolean isAuthError() {
        return getInstance().mAuthError;
    }
    public synchronized static void setAuthError() {
        getInstance().mAuthError = true;
    }
    public synchronized static void clearErrors() {
        getInstance().mAuthError = false;
    }

    public synchronized static JSONArray getPendingSurveys() {
        return getInstance().mSurveys;
    }

    public synchronized static Status getStatus() {
        SurveyDatabase.Status status = SurveyDatabase.Status.IDLE;
        synchronized (getInstance()) {
            status = getInstance().mStatus;
        }
        return status;
    }

    public synchronized static void update(Context ctx) {
        Log.d(TAG, "Updating...");
        ourInstance.loadFromStorage(ctx.getSharedPreferences(PREFS_KEY, 0));
    }

    public synchronized static void storeSurvey(Context ctx, SurveyData surveyData) {
        Log.d(TAG, "Storing new survey (count: " + getInstance().mSurveys.length() + ")");
        getInstance().mSurveys.put(surveyData.getData());
        getInstance().saveToStorage(ctx.getSharedPreferences(PREFS_KEY, 0));
        Log.d(TAG, "\tDone (count: " + getInstance().mSurveys.length() + ")");

//        if (Utils.hasActiveConnection(ctx)) {
//            Log.d(TAG, "Attempting to upload survey..");
//            getInstance().mStatus = Status.UPLOADING;
//            new PostSurveyTask(ctx).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, surveyData.getData());
//        }
    }

    public synchronized static void notifyUploaded(Context ctx, JSONObject obj) {
        Log.d(TAG, "Processing uploaded survey (count: " + getInstance().mSurveys.length() + ")");

        if (obj == null) {
            // Nothing to process. Just change status
            getInstance().mStatus = Status.IDLE;
            return;
        }

        Log.d(TAG, "\tObj (" + obj.getClass().getName() + "): " + obj);

        JSONArray newArr = new JSONArray();
        for (int i = 0; i < getInstance().mSurveys.length(); i++) {
            try {
                JSONObject survey = getInstance().mSurveys.getJSONObject(i);
                Log.d(TAG, "\tSurvey (" + survey.getClass().getName() + "): " + survey);
                Log.d(TAG, "\tObj-time " + obj.getJSONObject("completedTime").getString("time"));
                Log.d(TAG, "\tSurvey-time " + survey.getJSONObject("completedTime").getString("time"));
                if (!survey.getJSONObject("completedTime").getString("time").equals(
                        obj.getJSONObject("completedTime").getString("time"))) {
                    newArr.put(survey);
                } else {
                    Log.d(TAG, "\tRemoving survey at index " + i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (newArr.length() != (getInstance().mSurveys.length() - 1)) {
            getInstance().mStatus = Status.IDLE;
            throw new ArrayIndexOutOfBoundsException("Uploaded survey not found in storage!");
        }
        getInstance().mSurveys = newArr;
        getInstance().saveToStorage(ctx.getSharedPreferences(PREFS_KEY, 0));
        Log.d(TAG, "\tDone (count: " + getInstance().mSurveys.length() + ")");
        getInstance().mStatus = Status.IDLE;
    }

    public static int getSavedCount() {
        int count = 0;
        synchronized (getInstance()) {
            count = getInstance().mSurveys.length();
        }
        return count;
    }

    private static void uploadNext(Context ctx) {
        if (getInstance().mSurveys.length() > 0) {
            try {
                getInstance().mStatus = Status.UPLOADING;
                JSONObject survey = null;
                synchronized (getInstance()) {
                    survey = getInstance().mSurveys.getJSONObject(0);
                }
                Log.d(TAG, "Attempting to upload next survey..");
                new PostSurveyTask(ctx).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, survey);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void uploadSurveys(final Activity ctx, final Runnable postRunner) {
        if (SurveyDatabase.getSavedCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            builder.setMessage("Nothing to upload!");
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            return;
        }

        if (!Utils.hasActiveConnection(ctx)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            builder.setMessage("Not connected to the internet!");
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            return;
        }

        final AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            private ProgressDialog dialog;
            private int total;

            @Override
            protected void onPreExecute()
            {
                total = SurveyDatabase.getSavedCount();

                this.dialog = new ProgressDialog(ctx);
                this.dialog.setMessage("Uploading Surveys...");
                this.dialog.setCancelable(true);
                this.dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                this.dialog.setProgress(0);
                this.dialog.setMax(SurveyDatabase.getSavedCount());
                this.dialog.setOnCancelListener(new DialogInterface.OnCancelListener()
                {
                    @Override
                    public void onCancel(DialogInterface dialog)
                    {
                        // cancel AsyncTask
                        cancel(false);
                    }
                });
                this.dialog.show();

            }

            @Override
            protected Void doInBackground(Void... params)
            {
                SurveyDatabase.clearErrors();

                while (SurveyDatabase.getSavedCount() > 0) {
                    while (SurveyDatabase.getStatus() == SurveyDatabase.Status.UPLOADING) {
                        Thread.yield();
                    }

                    if (!SurveyDatabase.isAuthError()) {
                        // Start upload
                        Log.d(TAG, "Uploading a survey..");
                        dialog.setProgress(total - SurveyDatabase.getSavedCount());
                        SurveyDatabase.uploadNext(ctx);
                    } else {
                        Log.d(TAG, "Auth Error!");
                        break;
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result)
            {
                //called on ui thread
                if (this.dialog != null) {
                    this.dialog.dismiss();
                }

                if (SurveyDatabase.isAuthError()) {
                    // Clear the token
                    Settings.setToken(ctx, null);
                }

                if (postRunner != null) {
                    postRunner.run();
                }
            }

            @Override
            protected void onCancelled()
            {
                //called on ui thread
                if (this.dialog != null) {
                    this.dialog.dismiss();
                }

                if (postRunner != null) {
                    postRunner.run();
                }
            }
        };
        task.execute();
    }
}

class PostSurveyTask extends AsyncTask<JSONObject, Void, JSONObject> {

    private final Context mContext;
    private Exception exception;

    public PostSurveyTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected JSONObject doInBackground(JSONObject... surveys) {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;

        try {
            String url = Settings.getServerUrl() + "/dot/provider-surveys/submit";
            List<NameValuePair> getParams = new ArrayList<>();

            getParams.add( new BasicNameValuePair( "login", Settings.getUsername() ) );
            getParams.add( new BasicNameValuePair( "token", Settings.getToken() ) );

            url = url + "?" + URLEncodedUtils.format(getParams, "utf-8");
            HttpPost httppostreq = new HttpPost(url);

            StringEntity se = new StringEntity(surveys[0].toString());
            se.setContentType("application/json;charset=UTF-8");
            se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
            httppostreq.setEntity(se);
            response = httpclient.execute(httppostreq);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();
            }
            else if (statusLine.getStatusCode() == HttpStatus.SC_FORBIDDEN) {
                //Closes the connection.
                response.getEntity().getContent().close();
                Log.d(SurveyDatabase.TAG, "Cannot upload survey! " + statusLine.getReasonPhrase());
                //
                SurveyDatabase.setAuthError();
                //
                return null;
            }
            else {
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            Log.d(SurveyDatabase.TAG, "Encountered ClientProtocolException!" + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.d(SurveyDatabase.TAG, "Encountered IOException! " + e.getMessage());
            return null;
        }

        JSONObject ret = null;
        try {
            ret = new JSONObject(responseString).getJSONObject("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        if (jsonObject != null) {
            Log.d(SurveyDatabase.TAG, "Upload success..");
        } else {
            Log.d(SurveyDatabase.TAG, "Upload failure!");
        }

        SurveyDatabase.notifyUploaded(mContext, jsonObject);
    }
}