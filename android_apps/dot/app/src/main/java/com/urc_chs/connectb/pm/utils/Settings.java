/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import static com.urc_chs.connectb.pm.utils.Utils.Status;
import static com.urc_chs.connectb.pm.utils.Utils.DataValidity;

/**
 * Created by silvap on 2/26/15.
 */
public class Settings {
    public static String TAG = Utils.TAG + "/Settings";

    private static String PREFS_KEY = "Settings";
    private static String USERNAME = "username";
    private static String SERVER_URL = "serverUrl";
    private static String TOKEN = "token";

    private static Settings ourInstance = new Settings();

    public static Settings getInstance() {
        return ourInstance;
    }

    private volatile String username;
    private volatile String serverUrl;
    private volatile String token;

    private volatile String mErrorMsg = "";

    private volatile HashMap<String, String> mLoginAttempt = new HashMap<>(2);

    private volatile Status mStatus = Status.IDLE;
    private volatile DataValidity mValidity = DataValidity.DATA_INVALID;

    private Settings() {
    }

    private void loadFromStorage(SharedPreferences preferences) {
        Log.d(TAG, "Loading from storage...");

        username = preferences.getString(USERNAME, "");
        serverUrl = preferences.getString(SERVER_URL, "");
        token = preferences.getString(TOKEN, "");

        mValidity = (username.length() != 0 && serverUrl.length() != 0 && token.length() != 0)
                ? DataValidity.DATA_VALID : DataValidity.DATA_INVALID;
        Log.d(TAG, "Loaded: Username(" + username + ") " +
                "ServerURL(" + serverUrl + ") " + "Token(" + token + ")");
    }

    private void saveToStorage(SharedPreferences preferences) {
        Log.d(TAG, "Saving to storage...");
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(USERNAME, username);
        editor.putString(SERVER_URL, serverUrl);
        editor.putString(TOKEN, token);

        editor.commit();
        Log.d(TAG, "Saving completed");
    }

    public synchronized static Status getStatus() {
        return ourInstance.mStatus;
    }

    public synchronized static DataValidity getDataValidity() {
        return ourInstance.mValidity;
    }

    public synchronized static String getUsername() {
        return getInstance().username;
    }
    public synchronized static String getToken() {
        return getInstance().token;
    }
    public synchronized static String getServerUrl() {
        return getInstance().serverUrl;

//        if (Build.FINGERPRINT.contains("generic/vbox")) {
//            Log.d("Settings", "Returning URL for emulator");
//            return "http://192.168.56.1:3000";
//        } else {
//            Log.d("Settings", "Returning URL for device");
//            return "http://www.prasadsmind.info:3000";
//        }
    }

    public synchronized static String getError() {
        return ourInstance.mErrorMsg;
    }
    public synchronized static void setError(String errorMsg) {
        ourInstance.mErrorMsg = errorMsg;
    }

    public synchronized static void update(Context ctx) {
        Log.d(TAG, "Updating...");
        ourInstance.loadFromStorage(ctx.getSharedPreferences(PREFS_KEY, 0));
    }

    public synchronized static void setToken(Context ctx, String token) {
        Log.d(TAG, "Start set data..");

        if (token == null) {
            ourInstance.mValidity = DataValidity.DATA_INVALID;
            ourInstance.mStatus = Status.IDLE;
            // BAD LOGIN?
            return;
        }

        ourInstance.username = ourInstance.mLoginAttempt.get(USERNAME);
        ourInstance.serverUrl = ourInstance.mLoginAttempt.get(SERVER_URL);
        ourInstance.token = token;
        Log.d(TAG, "Setting new data: Username(" + ourInstance.username + ") " +
                "ServerURL(" + ourInstance.serverUrl + ") " + "Token(" + ourInstance.token + ")");

        ourInstance.saveToStorage(ctx.getSharedPreferences(PREFS_KEY, 0));

        ourInstance.mValidity = DataValidity.DATA_VALID;
        ourInstance.mStatus = Status.IDLE;
    }

    public static void attemptLogin(Context ctx, String username, String passcode, String serverUrl) {
        ourInstance.mErrorMsg = "";

        if (ourInstance.mStatus != Status.IDLE) {
            // Throw exception?
            return;
        }

        ourInstance.mLoginAttempt.clear();
        ourInstance.mLoginAttempt.put(USERNAME, username);
        ourInstance.mLoginAttempt.put(SERVER_URL, serverUrl);

        // Perform login attempt to retreive token
        Log.d(TAG, "Attempting Login...");
        ourInstance.mStatus = Status.UPDATING;
        new AttemptLoginTask(ctx, username, passcode, serverUrl).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}

class AttemptLoginTask extends AsyncTask<Void, Void, JSONObject> {

    private final Context mContext;
    private final String mUsername;
    private final String mPasscode;
    private final String mServerUrl;

    private String mErrorMsg;

    public AttemptLoginTask(Context context, String username, String passcode, String serverUrl) {
        this.mContext = context;
        this.mUsername = username;
        this.mPasscode = passcode;
        this.mServerUrl = serverUrl;

        this.mErrorMsg = "";
    }

    @Override
    protected JSONObject doInBackground(Void... params){

        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;

        try {
            String url = mServerUrl + "/dot/provider/signin";
            HttpPost httppostreq = new HttpPost(url);

            JSONObject loginData = new JSONObject();
            try {
                loginData.put("login", mUsername);
                loginData.put("passcode", mPasscode);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            StringEntity se = new StringEntity(loginData.toString());
            se.setContentType("application/json;charset=UTF-8");
            se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
            httppostreq.setEntity(se);
            response = httpclient.execute(httppostreq);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();
            }
            else if (statusLine.getStatusCode() == HttpStatus.SC_FORBIDDEN) {
                //Closes the connection.
                response.getEntity().getContent().close();
                Log.d(Settings.TAG, "Cannot login! " + statusLine.getReasonPhrase());
                mErrorMsg = "Incorrect Login!";
                return null;
            }
            else {
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch(IllegalStateException e) {
            Log.d(Settings.TAG, "Encountered IllegalStateException! " + e.getMessage());
            mErrorMsg = "Connection error!";
            return null;
        } catch (ClientProtocolException e) {
            Log.d(Settings.TAG, "Encountered ClientProtocolException! " + e.getMessage());
            mErrorMsg = "Connection error!";
            return null;
        } catch (IOException e) {
            Log.d(Settings.TAG, "Encountered IOException! " + e.getMessage());
            mErrorMsg = "Connection error!";
            return null;
        }

        JSONObject ret = null;
        try {
            ret = new JSONObject(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        Settings.setError(mErrorMsg);

        if (jsonObject == null) {
            Settings.setToken(mContext, null);
            return;
        }
        else {
            try {
                String token = jsonObject.getString("token");
                Settings.setToken(mContext, token);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}