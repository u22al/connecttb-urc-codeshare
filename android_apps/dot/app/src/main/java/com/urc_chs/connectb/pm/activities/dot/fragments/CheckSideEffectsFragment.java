/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;

public class CheckSideEffectsFragment extends BaseSurveyFragment {

    public static CheckSideEffectsFragment newInstance(SurveyData surveyData) {
        CheckSideEffectsFragment fragment = new CheckSideEffectsFragment();
        fragment.setSurveyData(surveyData);
        return fragment;
    }

    public CheckSideEffectsFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_dot_check_side_effects, container, false);

        final RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radioGroup);

        final Button button = (Button) rootView.findViewById(R.id.continueBtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.yes) {
                    mListener.OnHasSideEffects(mSurveyData);
                }
                else {
                    mListener.OnNoSideEffects(mSurveyData);
                }
            }
        });
        button.setEnabled(false);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                button.setEnabled(true);
            }
        });

        return rootView;
    }
}
