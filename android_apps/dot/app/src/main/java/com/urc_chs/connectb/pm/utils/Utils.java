/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by silvap on 1/10/15.
 */
public class Utils {

    public enum DataValidity {
        DATA_VALID,
        DATA_INVALID,
    }

    public enum Status {
        IDLE,
        UPDATING,
    }

    public static String TAG = "ConnecTB";

    public static boolean hasActiveConnection(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static JSONObject getSysInfo() throws JSONException {
        JSONObject sysInfo = new JSONObject();
        //
        sysInfo.put("board", Build.BOARD);
        sysInfo.put("brand", Build.BRAND);
        sysInfo.put("device", Build.DEVICE);
        sysInfo.put("display", Build.DISPLAY);
        sysInfo.put("hardware", Build.HARDWARE);
        sysInfo.put("manufacturer", Build.MANUFACTURER);
        sysInfo.put("model", Build.MODEL);
        sysInfo.put("product", Build.PRODUCT);
        sysInfo.put("serial", Build.SERIAL);
        sysInfo.put("fingerprint", Build.FINGERPRINT);
        sysInfo.put("bootloader", Build.BOOTLOADER);
        sysInfo.put("host", Build.HOST);
        sysInfo.put("id", Build.ID);
        sysInfo.put("tags", Build.TAGS);
        sysInfo.put("type", Build.TYPE);

        JSONObject version = new JSONObject();
        version.put("codename", Build.VERSION.CODENAME);
        version.put("incremental", Build.VERSION.INCREMENTAL);
        version.put("release", Build.VERSION.RELEASE);
        version.put("sdkInt", Build.VERSION.SDK_INT);
        sysInfo.put("version", version);

        //
        return sysInfo;
    }
}
