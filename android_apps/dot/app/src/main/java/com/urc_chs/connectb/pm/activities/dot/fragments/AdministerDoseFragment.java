/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.fragments;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;

import com.urc_chs.connectb.pm.activities.DotSurveyActivity;
import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.adapters.DosesListAdapter;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;
import com.urc_chs.connectb.pm.activities.dot.views.DosesListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AdministerDoseFragment extends BaseSurveyFragment {

    public static AdministerDoseFragment newInstance(SurveyData surveyData) {
        AdministerDoseFragment fragment = new AdministerDoseFragment();
        fragment.setSurveyData(surveyData);
        return fragment;
    }

    public AdministerDoseFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_dot_administer_dose, container, false);

        final Button continueBtn = (Button) rootView.findViewById(R.id.continueBtn);
        continueBtn.setEnabled(false);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.OnConfirmedDosesAdministered(mSurveyData);
            }
        });

        List<JSONObject> doses = new ArrayList<>();
        try {
            JSONArray dosesArray = mSurveyData.getDoses();
            for (int i = 0; i < dosesArray.length(); i++) {
                doses.add(dosesArray.getJSONObject(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final DosesListAdapter dosesListAdapter = new DosesListAdapter(getActivity(), doses);

        final DosesListView dosesList = (DosesListView) rootView.findViewById(R.id.doseList);
        dosesList.setDivider(null);
        dosesList.setAdapter(dosesListAdapter);
        dosesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
                if (checkBox.isChecked()) return;

                checkBox.setChecked(true);
                checkBox.setAlpha(0.4f);

                try {
                    DotSurveyActivity activity = (DotSurveyActivity) getActivity();
                    Location location = activity.getLastAcquiredLocation();
                    if (location == null) throw new NullPointerException("Location cannot be null!");

                    mSurveyData.administerDose(position, checkBox.isChecked(), location);
                    if (mSurveyData.allDosesAdministered()) {
                        continueBtn.setEnabled(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dosesListAdapter.notifyDataSetChanged();

                setContinueButtonLabel(continueBtn, dosesListAdapter);
            }

        });

        setContinueButtonLabel(continueBtn, dosesListAdapter);

        return rootView;
    }

    private void setContinueButtonLabel(Button continueBtn, DosesListAdapter doses) {
        int numAdministered = doses.numAdministered();
        int totalDoses = doses.getCount();

        if (numAdministered == totalDoses) {
            continueBtn.setText( getResources().getString(R.string.continue_caps) );
        }
        else {
            continueBtn.setText(String.format("%d%% Completed", (int)((numAdministered / (float)totalDoses) * 100)));
        }
    }
}
