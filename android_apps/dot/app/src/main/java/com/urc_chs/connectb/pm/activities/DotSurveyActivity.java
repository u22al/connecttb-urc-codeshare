/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyDatabase;
import com.urc_chs.connectb.pm.activities.dot.fragments.AdministerDoseFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.AttentionFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.CheckChildFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.CheckSickAdultFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.CheckSideEffectsFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.ConfirmFinancialAssistanceFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.ReportChildFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.ReportPreviousFacilityVisitFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.ReportSickAdultFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.ReportSideEffectsFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.SideEffectManagementFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.SurveyCompletedFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.SurveySubmitFragment;
import com.urc_chs.connectb.pm.activities.dot.listeners.SurveyListener;
import com.urc_chs.connectb.pm.utils.Utils;
import com.urc_chs.connectb.pm.utils.location.FallbackLocationTracker;
import com.urc_chs.connectb.pm.utils.location.LocationTracker;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Created by silvap on 1/9/15.
 */
public class DotSurveyActivity
        extends
            ActionBarActivity
        implements
            SurveyListener, LocationTracker.LocationUpdateListener
{
    private static String TAG = Utils.TAG + "/DotSurvey";

    public final static String INTENT_ARG_SURVEY_DATA = "surveyData";

    private FallbackLocationTracker locationTracker = null;
    private Location savedLocation = null;
    private boolean skipSensorCheck = false;
    private boolean firstCheck = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        locationTracker = new FallbackLocationTracker(this);

        setContentView(R.layout.activity_connectb);

        initialize(savedInstanceState);
    }

    private void initialize(Bundle savedInstanceState) {

        if (com.urc_chs.connectb.pm.utils.Settings.getDataValidity() == Utils.DataValidity.DATA_INVALID) {
            // Goto login activity
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return;
        }

        SurveyDatabase.update(this);

        String json = getIntent().getStringExtra(INTENT_ARG_SURVEY_DATA);
        JSONObject data = null;
        try {
            data = (JSONObject) new JSONTokener(json).nextValue();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SurveyData surveyData = new SurveyData(true, data);

        locationTracker.start(this);

        checkSensorStatus(new Runnable() {
            private Bundle savedInstanceState;
            private SurveyData surveyData;

            public Runnable init(Bundle savedInstanceState, SurveyData surveyData) {
                this.savedInstanceState = savedInstanceState;
                this.surveyData = surveyData;
                return this;
            }

            @Override
            public void run() {

                try {
                    surveyData.startSurvey(getLastAcquiredLocation());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String patientName = null;
                try {
                    patientName = surveyData.getSurveyProfile().getJSONObject("patient").getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setTitle(String.format(getString(R.string.dot_for_patient), patientName));

                if (savedInstanceState == null) {
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.container, AttentionFragment.newInstance(surveyData))
                            .commit();
                }

            }
        }.init(savedInstanceState, surveyData));
    }

    protected boolean hasGPS() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return lm.getProvider(LocationManager.GPS_PROVIDER) != null;
    }

    protected boolean isGPSEnabled() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationProvider lp = lm.getProvider(LocationManager.GPS_PROVIDER);
        return lp != null && lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    protected void checkSensorStatus(final Runnable postRunner) {
        if (!hasGPS()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("GPS not available!");
            builder.setMessage("The device does not have GPS support. Cannot complete survey!");
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    endSurveyActivity();
                }
            });
            dialog.show();
            return;
        }

        boolean sensorsOk = isGPSEnabled();
        if (sensorsOk) {

            // Location is null; spin until we get a valid one
            if (getLastAcquiredLocation() == null) {

                final String message = "Acquiring sensor data. (%d)\n\nIf you are indoors, then you may need to go outside to acquire a valid GPS signal.";
                final ProgressDialog ringProgressDialog = ProgressDialog.show(this,
                        "Please wait ...",
                        message
                        , true);

                final Runnable acquireErrorRunner = new Runnable() {
                    @Override
                    public void run() {
                        showGPSError(postRunner);
                    }
                };
                final long startTime = System.currentTimeMillis();
                final long max = (1000 * 120);
                final Runnable setMessageRunner = new Runnable() {
                    @Override
                    public void run() {
                        long duration = System.currentTimeMillis() - startTime;
                        ringProgressDialog.setMessage(String.format(message, Math.round((max - duration) / 1000)));
                    }
                };

                new Thread(new Runnable() {
                    private DotSurveyActivity activity = null;

                    public Runnable init(DotSurveyActivity activity) {
                        this.activity = activity;
                        return this;
                    }
                    @Override
                    public void run() {
                        boolean fail = false;
                        long lastSec = Math.round(max / 1000);

                        Location acquiredLocation = activity.getLastAcquiredLocation();
                        while (acquiredLocation == null ||
                                (firstCheck && acquiredLocation.getAccuracy() > 100)) {
                            // Spin
                            Thread.yield();

                            long duration = System.currentTimeMillis() - startTime;
                            if (lastSec != Math.round((max - duration) / 1000)) {
                                lastSec = Math.round((max - duration) / 1000);
                                activity.runOnUiThread(setMessageRunner);
                            }
                            if (duration > max) {
                                if (acquiredLocation == null) {
                                    fail = true;
                                }
                                break;
                            }

                            acquiredLocation = activity.getLastAcquiredLocation();
                        }
                        firstCheck = false;

                        // Do NOT allow stale location. Stale locations provide no value.
                        //allowStaleLocation = true;

                        ringProgressDialog.dismiss();

                        if (!fail && postRunner != null) {
                            activity.runOnUiThread(postRunner);
                        }

                        if (fail) {
                            activity.runOnUiThread(acquireErrorRunner);
                        }
                    }
                }.init(this)).start();

                return;
            }
            else {
                if (postRunner != null) {
                    postRunner.run();
                }
            }
        }
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("GPS is disabled!");
            builder.setMessage("Please enable GPS to complete survey.");
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    endSurveyActivity();
                }
            });
            dialog.show();
        }
    }

    protected void cleanup() {
        locationTracker.stop();
    }

    protected void endSurveyActivity() {
        cleanup();

        // Go back to the main activity
        startActivity(new Intent(this, PatientSelectionActivity.class));
        finish();
    }

    protected void showGPSError(final Runnable postRunner) {
        boolean retry = false;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Unable to detect location!");
        builder.setMessage("Retry (recommended) or Continue without valid geotag?");
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                skipSensorCheck = true;
                if (postRunner != null) {
                    postRunner.run();
                }
            }
        });
        builder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                checkSensorStatus(postRunner);
            }
        });
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public synchronized Location getLastAcquiredLocation() {
        Location loc = savedLocation;
        // If we are skipping sensor check, then create an invalid location
        if (loc == null && skipSensorCheck) {
            loc = new Location(LocationManager.PASSIVE_PROVIDER);
        }
        return loc;
    }

    @Override
    public void onUpdate(Location oldLoc, long oldTime, Location newLoc, long newTime) {
        Log.d(TAG, "Location updated: " + newLoc.toString());
        savedLocation = newLoc;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.exit_survey));
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                endSurveyActivity();
                }
            });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

            return;
        }
        super.onBackPressed();
    }

    @Override
    public void OnAttentionConfirmed(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = AdministerDoseFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.commit();
    }

    @Override
    public void OnConfirmedDosesAdministered(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = CheckSideEffectsFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.commit();
    }

    @Override
    public void OnHasSideEffects(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = ReportSideEffectsFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnReportedAllSideEffects(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = SideEffectManagementFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnReportedSideEffectsManagement(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = ReportPreviousFacilityVisitFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnNoSideEffects(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = ReportPreviousFacilityVisitFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnReportedPreviousFacilityVisit(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = CheckSickAdultFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnHasSickAdult(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = ReportSickAdultFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnReportedAdultFacilityVisit(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = CheckChildFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnNoSickAdult(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = CheckChildFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnHasChild(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = ReportChildFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnReportedChildFacilityVisit(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = ConfirmFinancialAssistanceFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnNoChild(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = ConfirmFinancialAssistanceFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnConfirmedFinancialAssistance(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        Fragment newFragment = SurveySubmitFragment.newInstance(data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnSubmitDOT(SurveyData data) {
        if (!skipSensorCheck) {
            checkSensorStatus(null);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.submit_survey));
        builder.setMessage(getString(R.string.submit_survey_message));
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            private SurveyData data;
            public DialogInterface.OnClickListener init(SurveyData data) {
                this.data = data;
                return this;
            }
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

                completeSurvey(data);

                Fragment newFragment = SurveyCompletedFragment.newInstance(data);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
                    getSupportFragmentManager().popBackStack();
                }
                transaction.replace(R.id.container, newFragment);
                transaction.commit();
            }
        }.init(data));
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void OnRequestedExit(SurveyData data) {
        finish();
    }

    @Override
    public void OnRequestedAnotherSurvey(SurveyData data) {
        endSurveyActivity();
    }

    private void completeSurvey(SurveyData data) {
        Location location = getLastAcquiredLocation();
        if (location == null)
            throw new NullPointerException("Location cannot be null!");

        try {
            data.complete(location);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SurveyDatabase.storeSurvey(this, data);

        if (Utils.hasActiveConnection(this)) {
            SurveyDatabase.uploadSurveys(this, new Runnable() {
                @Override
                public void run() {
                    cleanup();
                }
            });
        }
    }
}
