/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.login.fragments;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;

import com.urc_chs.connectb.pm.activities.login.listeners.LoginProcessListener;

/**
 * Created by silvap on 4/4/15.
 */
public class BaseLoginFragment extends Fragment {

    protected LoginProcessListener mListener = null;

    protected Handler buttonFader = new Handler();

    public BaseLoginFragment() {
        super();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (LoginProcessListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement SurveyListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
