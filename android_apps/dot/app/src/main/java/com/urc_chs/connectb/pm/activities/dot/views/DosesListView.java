/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.views;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by silvap on 1/13/15.
 */
public class DosesListView extends ListView {

    public DosesListView(Context context) {
        super(context);
    }

    // For System
    public DosesListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    // For Layout Editor
    public DosesListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    // We are explicitly not saving/restoring view state
    @Override
    public Parcelable onSaveInstanceState()
    {
        super.onSaveInstanceState();
        return null;
    }

    // We are explicitly not saving/restoring view state
    @Override
    public void onRestoreInstanceState(Parcelable state)
    {
        super.onRestoreInstanceState(null);
    }
}
