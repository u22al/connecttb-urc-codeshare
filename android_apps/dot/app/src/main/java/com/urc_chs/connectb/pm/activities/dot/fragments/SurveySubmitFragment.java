/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;

import org.json.JSONException;

/**
 * A simple {@link Fragment} subclass.
 */
public class SurveySubmitFragment extends BaseSurveyFragment {


    public static SurveySubmitFragment newInstance(SurveyData surveyData) {
        SurveySubmitFragment fragment = new SurveySubmitFragment();
        fragment.setSurveyData(surveyData);
        return fragment;
    }

    public SurveySubmitFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_dot_survey_sumbit, container, false);

        final Button submitBtn = (Button) rootView.findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!mSurveyData.isCompleted()) {
                        mListener.OnSubmitDOT(mSurveyData);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        submitBtn.setEnabled(false);
        buttonFader.postDelayed(new Runnable() {
            @Override
            public void run() {
                submitBtn.setEnabled(true);
            }
        }, 1000);

        return rootView;
    }


}
