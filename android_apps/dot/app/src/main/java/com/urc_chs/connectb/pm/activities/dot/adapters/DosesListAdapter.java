/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by silvap on 1/9/15.
 */
public class DosesListAdapter extends ArrayAdapter<JSONObject> {

    public DosesListAdapter(Activity context, List<JSONObject> doses) {
        super(context, R.layout.view_administer_dose_checkbox, doses);
    }

    public int numAdministered() {
        int administeredCount = 0;
        for (int i = 0; i < getCount(); i++) {
            JSONObject dose = getItem(i);
            try {
                if (dose.getBoolean(SurveyData.KEY_ADMINISTERED)) {
                    administeredCount++;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return administeredCount;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        JSONObject dose = getItem(position);

        View vi = convertView;

        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.view_administer_dose_checkbox, parent, false);
        }

        String label = "???";
        boolean administered = false;
        try {
            label = dose.getString(SurveyData.KEY_DRUG);
            administered = dose.getBoolean(SurveyData.KEY_ADMINISTERED);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CheckBox doseCheckbox = (CheckBox) vi.findViewById(R.id.checkBox);
        doseCheckbox.setText(label);
        doseCheckbox.setChecked(administered);

        return vi;
    }

    @Override
    public boolean isEnabled(int position) {
        JSONObject dose = getItem(position);
        boolean administered = false;
        try {
            administered = dose.getBoolean(SurveyData.KEY_ADMINISTERED);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return !administered;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }
}
