/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.login.fragments.LoginFragment;
import com.urc_chs.connectb.pm.activities.login.fragments.PrivacyConfirmFragment;
import com.urc_chs.connectb.pm.activities.login.listeners.LoginProcessListener;
import com.urc_chs.connectb.pm.utils.Settings;
import com.urc_chs.connectb.pm.utils.Utils;

public class LoginActivity
        extends
            ActionBarActivity
        implements
        LoginProcessListener
{

    private static String TAG = Utils.TAG + "/Login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Settings.update(this);

        setContentView(R.layout.activity_connectb);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, LoginFragment.newInstance())
                    .commit();
        }
    }

    @Override
    public void OnLoginSuccess() {
        Fragment newFragment = PrivacyConfirmFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnPrivacyConfirm() {
        finish();
    }
}
