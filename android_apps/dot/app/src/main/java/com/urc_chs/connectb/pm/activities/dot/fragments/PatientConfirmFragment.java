/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.fragments;

import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PatientConfirmFragment
        extends
        BasePatientFragment
{
    public static PatientConfirmFragment newInstance(SurveyData surveyData) {
        PatientConfirmFragment fragment = new PatientConfirmFragment();
        fragment.setSurveyData(surveyData);
        return fragment;
    }

    public PatientConfirmFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_dot_patient_confirm, container, false);

        JSONObject patient = null;
        try {
            patient = mSurveyData.getSurveyProfile().getJSONObject("patient");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView name = (TextView) rootView.findViewById(R.id.drug);
        try {
            name.setText(patient.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView gender = (TextView) rootView.findViewById(R.id.gender);
        try {
            gender.setText(patient.getString("gender"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView dob = (TextView) rootView.findViewById(R.id.dob);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format.parse(patient.getString("dob"));
            dob.setText(DateFormat.getDateFormat(getActivity()).format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView phone = (TextView) rootView.findViewById(R.id.phoneNumber);
        try {
            phone.setText(patient.getString("phone"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject household = new JSONObject();
        try {
            household = patient.getJSONObject("household");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView adults = (TextView) rootView.findViewById(R.id.adults);
        try {
            adults.setText("Adults: " + Integer.toString(household.getInt("maleAdults")) + " Males, " +
                Integer.toString(household.getInt("femaleAdults")) + " Females");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView children = (TextView) rootView.findViewById(R.id.children);
        try {
            children.setText("Children: " + Integer.toString(household.getInt("maleChildren")) + " Males, " +
                    Integer.toString(household.getInt("femaleChildren")) + " Females");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Button okBtn = (Button) rootView.findViewById(R.id.okBtn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.OnPatientConfirmOK(mSurveyData);
            }
        });

        Button cancelBtn = (Button) rootView.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.OnPatientConfirmCancel();
            }
        });

        return rootView;
    }
}
