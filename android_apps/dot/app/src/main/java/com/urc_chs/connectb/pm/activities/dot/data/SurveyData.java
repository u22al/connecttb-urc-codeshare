/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.data;

import android.location.Location;

import com.urc_chs.connectb.pm.utils.TimestampUtils;
import com.urc_chs.connectb.pm.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by silvap on 1/11/15.
 */
public class SurveyData {

    private final static String KEY_APP_VERSION = "appVersion";
    private final static String KEY_SYS_INFO = "sysInfo";
    private final static String KEY_TIMESTAMP = "timestamp";

    public final static String KEY_SURVEY_PROFILE = "surveyProfile";

    public final static String KEY_DOSES = "doses";

    public final static String KEY_START_TIME = "startTime";
    public final static String KEY_COMPLETED_TIME = "completedTime";

    public final static String KEY_ADMINISTERED = "administered";
    public final static String KEY_DRUG = "drug";

    public final static String KEY_SIDE_EFFECTS = "sideEffects";
    public final static String KEY_SCREENING_QUESTION = "screeningQuestion";
    public final static String KEY_REPORTED = "reported";
    public final static String KEY_REPORTED_TIME = "reportedTime";

    // Geo Tag related keys
    public final static String KEY_LONGITUDE = "longitude";
    public final static String KEY_LATITUDE = "latitude";
    public final static String KEY_ACCURACY = "accuracy";
    private static final String KEY_ALTITUDE = "altitude";
    private static final String KEY_BEARING = "bearing";
    private static final String KEY_LOCATION_TIME = "locationTime";
    private static final String KEY_TIME = "time";

    private static final String KEY_STARTED = "started";
    private static final String KEY_COMPLETED = "completed";

    private static final String KEY_HEADACHE = "headache";
    private static final String KEY_NAUSEA = "nausea";
    private static final String KEY_NEAREST_CLINIC = "nearestClinic";
    private static final String KEY_HOSPITAL = "hospital";
    private static final String KEY_SIDE_EFFECT_MANAGEMENT = "sideEffectManagement";

    private static final String KEY_MONTHLY_DATA = "monthlyData";
    // BEGIN MONTHLY DATA
    private static final String KEY_VISITED = "visited";
    private static final String KEY_PREVIOUS_FACILITY_VISIT = "previousFacilityVisit";

    private static final String KEY_SICK_ADULT = "sickAdult";
    private static final String KEY_SICK_ADULT_IN_FAMILY = "sickAdultInFamily";

    private static final String KEY_HAS_CHILD = "hasChild";
    private static final String KEY_HAS_CHILD_IN_FAMILY = "hasChildInFamily";

    public final static String KEY_FINANCIAL_ASSISTANCE = "financialAssistance";
    public final static String KEY_RECEIVED = "received";
    private static final String KEY_PROVIDER = "provider";
    // END MONTHLY DATA

    private JSONObject mData = null;

    public static SurveyData newInstance(String version, JSONObject profile) throws JSONException {
        SurveyData surveyData = new SurveyData(version, profile);
        return surveyData;
    }

    // TODO Clean this usage pattern
    public SurveyData(boolean restore, JSONObject data) {
        mData = data;
    }

    public SurveyData(String version, JSONObject profile) throws JSONException {
        // TODO remove check param
        mData = new JSONObject();
        mData.put(KEY_APP_VERSION, version);
        mData.put(KEY_SYS_INFO, Utils.getSysInfo());
        mData.put(KEY_STARTED, false);
        mData.put(KEY_COMPLETED, false);
        mData.put(KEY_SURVEY_PROFILE, profile);
    }

    public JSONObject getData() { return mData; }

    @Override
    public String toString() {
        return mData.toString();
    }

    public JSONObject getSurveyProfile() throws JSONException {
        return mData.getJSONObject(KEY_SURVEY_PROFILE);
    }

    public void startSurvey(Location location) throws JSONException {
        if (isStarted()) throw new IllegalStateException("Survey has already been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONObject profile = getSurveyProfile();

        // Setup Doses
        JSONArray doses = new JSONArray();
        JSONArray medications = profile.getJSONArray("medications");
        int index = 0;
        for (int i = 0; i < medications.length(); i++) {
            addDose(doses, index, medications.getString(i));
            index++;
        }
        mData.put(KEY_DOSES, doses);

        // Setup Side Effects
        JSONArray sideEffects = SurveyDefnsDatabase.getAllSideEffects();
        index = 0;
        for (int i = 0; i < sideEffects.length(); i++) {
            addSideEffect(sideEffects, index, sideEffects.getJSONObject(i));
            index++;
        }
        mData.put(KEY_SIDE_EFFECTS, sideEffects);

        // Start timestamp
        addTimestamp(mData, KEY_START_TIME, location);

        mData.put(KEY_STARTED, true);
    }

    public void administerDose(int doseIndex, boolean administered, Location location) throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONArray doses = getDoses();
        JSONObject dose = (JSONObject) doses.get(doseIndex);
        dose.put(KEY_ADMINISTERED, administered);
        addTimestamp(dose, KEY_TIMESTAMP, location);
    }

    public boolean allDosesAdministered() throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");

        JSONArray doses = getDoses();
        for (int i = 0; i < doses.length(); i++) {
            JSONObject dose = doses.getJSONObject(i);
            if (!dose.getBoolean(KEY_ADMINISTERED))
                return false;
        }
        return true;
    }

    public void reportSideEffect(int sideEffectIndex, boolean reported, Location location) throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONArray sideEffects = getSideEffects();
        JSONObject sideEffect = (JSONObject) sideEffects.get(sideEffectIndex);

        sideEffect.put(KEY_REPORTED, reported);

        addTimestamp(sideEffect, KEY_REPORTED_TIME, location);
    }

    public void setSideEffectManagement(boolean headache, boolean nausea, boolean nearestClinic,
                                        boolean hospital, Location location) throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONObject sideEffectManagement = new JSONObject();

        sideEffectManagement.put(KEY_HEADACHE, headache);
        sideEffectManagement.put(KEY_NAUSEA, nausea);
        sideEffectManagement.put(KEY_NEAREST_CLINIC, nearestClinic);
        sideEffectManagement.put(KEY_HOSPITAL, hospital);

        addTimestamp(sideEffectManagement, KEY_TIMESTAMP, location);

        mData.put(KEY_SIDE_EFFECT_MANAGEMENT, sideEffectManagement);
    }

    public void setPreviousFacilityVisit(boolean value, Location location) throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONObject previousFacilityVisit = new JSONObject();

        previousFacilityVisit.put(KEY_VISITED, value);

        addTimestamp(previousFacilityVisit, KEY_TIMESTAMP, location);

        getMonthlyData().put(KEY_PREVIOUS_FACILITY_VISIT, previousFacilityVisit);
    }

    public void setSickAdultInFamily(boolean value, Location location) throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONObject sickAdultInFamily = new JSONObject();

        sickAdultInFamily.put(KEY_SICK_ADULT, value);

        addTimestamp(sickAdultInFamily, KEY_TIMESTAMP, location);

        getMonthlyData().put(KEY_SICK_ADULT_IN_FAMILY, sickAdultInFamily);
    }

    public void setSickAdultVisitedFacility(boolean value, Location location) throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONObject sickAdultInFamily = getMonthlyData().getJSONObject(KEY_SICK_ADULT_IN_FAMILY);

        sickAdultInFamily.put(KEY_VISITED, value);
    }

    public void setHasChildInFamily(boolean value, Location location) throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONObject hasChildInFamily = new JSONObject();

        hasChildInFamily.put(KEY_HAS_CHILD, value);

        addTimestamp(hasChildInFamily, KEY_TIMESTAMP, location);

        getMonthlyData().put(KEY_HAS_CHILD_IN_FAMILY, hasChildInFamily);
    }

    public void setChildBroughtToFacility(boolean value, Location location) throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONObject hasChildInFamily = getMonthlyData().getJSONObject(KEY_HAS_CHILD_IN_FAMILY);

        hasChildInFamily.put(KEY_VISITED, value);
    }

    public void setReceivedFinancialAssistance(boolean value, Location location) throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONObject financialAssistance = new JSONObject();

        financialAssistance.put(KEY_RECEIVED, value);

        addTimestamp(financialAssistance, KEY_TIMESTAMP, location);

        getMonthlyData().put(KEY_FINANCIAL_ASSISTANCE, financialAssistance);
    }

    public void complete(Location location) throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        addTimestamp(mData, KEY_COMPLETED_TIME, location);

        mData.put(KEY_COMPLETED, true);
    }

    public boolean isStarted() throws JSONException {
        return mData.getBoolean(KEY_STARTED);
    }
    public boolean isCompleted() throws JSONException {
        return mData.getBoolean(KEY_COMPLETED);
    }

    public JSONArray getDoses() throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");

        return mData.getJSONArray(KEY_DOSES);
    }
    public JSONArray getSideEffects() throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");

        return mData.getJSONArray(KEY_SIDE_EFFECTS);
    }

    public JSONObject getMonthlyData() throws JSONException {
        if (!isStarted()) throw new IllegalStateException("Survey has not been started!");

        JSONObject monthlyData = null;

        if (mData.has(KEY_MONTHLY_DATA)) {
            monthlyData = mData.getJSONObject(KEY_MONTHLY_DATA);
        }
        else {
            monthlyData = new JSONObject();
            mData.put(KEY_MONTHLY_DATA, monthlyData);
        }

        return monthlyData;
    }

    private void addDose(JSONArray doses, int index, String medication) throws JSONException {
        JSONObject dose = new JSONObject();
        dose.put(KEY_DRUG, medication);
        dose.put(KEY_ADMINISTERED, false);
        dose.put(KEY_TIMESTAMP, 0);
        doses.put(index, dose);
    }

    private void addSideEffect(JSONArray sideEffects, int index, JSONObject data) throws JSONException {
        JSONObject sideEffect = new JSONObject(data.toString());
        sideEffect.put(KEY_REPORTED, false);
        sideEffect.put(KEY_REPORTED_TIME, 0);
        //
        sideEffects.put(index, sideEffect);
    }

    private void addTimestamp(JSONObject object, String key, Location location) throws JSONException {
        object.put(key, generateGeoTag(location));
    }

    private JSONObject generateGeoTag(Location location) throws JSONException {
        if (isCompleted()) throw new IllegalStateException("Survey has already been completed!");

        JSONObject geotag = new JSONObject();
        geotag.put(KEY_PROVIDER, location.getProvider());
        geotag.put(KEY_LONGITUDE, location.getLongitude());
        geotag.put(KEY_LATITUDE, location.getLatitude());
        geotag.put(KEY_ALTITUDE, location.getAltitude());
        geotag.put(KEY_ACCURACY, location.getAccuracy());
        geotag.put(KEY_BEARING, location.getBearing());
        geotag.put(KEY_LOCATION_TIME, TimestampUtils.getISO8601StringForDate(new Date(location.getTime())));
        geotag.put(KEY_TIME, TimestampUtils.getISO8601StringForCurrentDate());

        return geotag;
    }
}
