/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.SearchView;
import android.widget.TextView;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.adapters.ExpandingPatientsListAdapter;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyDefnsDatabase;
import com.urc_chs.connectb.pm.activities.dot.views.PatientsListView;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by silvap on 1/10/15.
 */
public class PatientSelectorFragment
        extends
        BasePatientFragment
        implements
            ExpandingPatientsListAdapter.OnPatientVisitListener
{
    TextView mNoResultsMsg = null;
    PatientsListView mPatientsList = null;
    SearchView mSearchView = null;
    int mLastExpandedPatientListItemIndex = -1;
    ExpandingPatientsListAdapter mPatientsListAdapter = null;
    Filter.FilterListener mPatientsFilterListener = new Filter.FilterListener() {
        @Override
        public void onFilterComplete(int count) {
            boolean emptyPatientsList = (count == 0);
            mPatientsList.setAlpha(emptyPatientsList ? 0 : 1);
            mNoResultsMsg.setAlpha(emptyPatientsList ? 1 : 0);
        }
    };

    public static PatientSelectorFragment newInstance() {
        return new PatientSelectorFragment();
    }

    public PatientSelectorFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        final View rootView = inflater.inflate(R.layout.fragment_dot_patient_selector, container, false);

        mNoResultsMsg = (TextView) rootView.findViewById(R.id.noPatientsMsg);
        mNoResultsMsg.setAlpha(0);

        final JSONArray surveyDefns = SurveyDefnsDatabase.getAllProfiles();
        mPatientsListAdapter = new ExpandingPatientsListAdapter(getActivity(), surveyDefns, this);

        mPatientsList = (PatientsListView) rootView.findViewById(R.id.patientsList);
        mPatientsList.setChildDivider(null);
        mPatientsList.setDivider(null);
        mPatientsList.setGroupIndicator(null);
        mPatientsList.setChildIndicator(null);
        mPatientsList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (mLastExpandedPatientListItemIndex != -1
                        && groupPosition != mLastExpandedPatientListItemIndex) {
                    mPatientsList.collapseGroup(mLastExpandedPatientListItemIndex);
                }
                mLastExpandedPatientListItemIndex = groupPosition;
            }
        });
        mPatientsList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                mLastExpandedPatientListItemIndex = -1;
            }
        });
        mPatientsList.setAdapter(mPatientsListAdapter);
        mPatientsList.requestFocus();

        mSearchView = (SearchView) rootView.findViewById(R.id.searchView);
        mSearchView.clearFocus();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                doFilter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                doFilter(newText);
                return false;
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPatientsList.requestFocus();
    }

    @Override
    public void onPatientVisit(JSONObject profile) {
        mListener.OnDotSurveyStarted(profile);
    }

    private void doFilter(String query) {
        mPatientsListAdapter.getFilter().filter(query, mPatientsFilterListener);
        collapseExpandedPatientListItem();
    }

    private void collapseExpandedPatientListItem() {
        if (mLastExpandedPatientListItemIndex != -1) {
            mPatientsList.collapseGroup(mLastExpandedPatientListItemIndex);
        }
    }
}
