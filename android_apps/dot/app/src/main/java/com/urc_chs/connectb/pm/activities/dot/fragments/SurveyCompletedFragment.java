/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;

public class SurveyCompletedFragment extends BaseSurveyFragment {

    public static SurveyCompletedFragment newInstance(SurveyData surveyData) {
        SurveyCompletedFragment fragment = new SurveyCompletedFragment();
        fragment.setSurveyData(surveyData);
        return fragment;
    }

    public SurveyCompletedFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_dot_survey_completed, container, false);

        final Button anotherDotBtn = (Button) rootView.findViewById(R.id.anotherDotBtn);
        anotherDotBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.OnRequestedAnotherSurvey(mSurveyData);
            }
        });
        anotherDotBtn.setEnabled(false);

        final Button exitbutton = (Button) rootView.findViewById(R.id.exitBtn);
        exitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.OnRequestedExit(mSurveyData);
            }
        });
        exitbutton.setEnabled(false);

        buttonFader.postDelayed(new Runnable() {
            @Override
            public void run() {
                anotherDotBtn.setEnabled(true);
                exitbutton.setEnabled(true);
            }
        }, 1000);

        return rootView;
    }

}
