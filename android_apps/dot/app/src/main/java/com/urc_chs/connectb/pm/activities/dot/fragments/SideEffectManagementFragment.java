/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.fragments;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;

import com.urc_chs.connectb.pm.activities.DotSurveyActivity;
import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;

import org.json.JSONException;

public class SideEffectManagementFragment extends BaseSurveyFragment {

    public static SideEffectManagementFragment newInstance(SurveyData surveyData) {
        SideEffectManagementFragment fragment = new SideEffectManagementFragment();
        fragment.setSurveyData(surveyData);
        return fragment;
    }

    public SideEffectManagementFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_dot_side_effect_management, container, false);

        final RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radioGroup);

        final Button button = (Button) rootView.findViewById(R.id.continueBtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean nearestClinic = (radioGroup.getCheckedRadioButtonId() == R.id.radioNearestClinic);
                boolean hospital = (radioGroup.getCheckedRadioButtonId() == R.id.radioHospital);
                boolean headache = ((CheckBox) rootView.findViewById(R.id.checkHeadache)).isChecked();
                boolean nausea = ((CheckBox) rootView.findViewById(R.id.checkNausea)).isChecked();
                try {
                    DotSurveyActivity activity = (DotSurveyActivity) getActivity();
                    Location location = activity.getLastAcquiredLocation();
                    mSurveyData.setSideEffectManagement(
                            headache, nausea, nearestClinic, hospital, location);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mListener.OnReportedSideEffectsManagement(mSurveyData);
            }
        });
        button.setEnabled(false);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                button.setEnabled(true);
            }
        });

        return rootView;
    }
}