/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.login.listeners;

/**
 * Created by silvap on 4/4/15.
 */
public interface LoginProcessListener {
    public void OnLoginSuccess();
    public void OnPrivacyConfirm();
}
