/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.login.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.LoginActivity;
import com.urc_chs.connectb.pm.utils.Settings;
import com.urc_chs.connectb.pm.utils.Utils;

public class LoginFragment extends BaseLoginFragment {

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    public LoginFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final LoginActivity activity = (LoginActivity) getActivity();

        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        final EditText username = (EditText) rootView.findViewById(R.id.username);
        username.setText(Settings.getUsername());
        final EditText passcode = (EditText) rootView.findViewById(R.id.passcode);
        final EditText serverUrl = (EditText) rootView.findViewById(R.id.serverUrl);
        serverUrl.setText(Settings.getServerUrl());

        final Button continueButton = (Button) rootView.findViewById(R.id.loginButton);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utils.hasActiveConnection(activity)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Not connected to the internet!");
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return;
                }

                Settings.attemptLogin(
                        activity,
                        username.getText().toString(),
                        passcode.getText().toString(),
                        serverUrl.getText().toString()
                );

                final Runnable postRunner = new Runnable() {
                    @Override
                    public void run() {
                        if (!Settings.getError().isEmpty()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle("Error");
                            builder.setMessage(Settings.getError());
                            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.setCancelable(false);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                        else {
                            mListener.OnLoginSuccess();
                        }
                    }
                };

                final ProgressDialog ringProgressDialog = ProgressDialog.show(
                        getActivity(),
                        "Please wait ...",
                        "Authenticating", true, false);
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        while (Settings.getStatus() == Utils.Status.UPDATING) {
                            // Spin
                            Thread.yield();
                        }

                        ringProgressDialog.dismiss();

                        if (postRunner != null) {
                            activity.runOnUiThread(postRunner);
                        }
                    }
                }).start();
            }
        });

        return rootView;
    }

}
