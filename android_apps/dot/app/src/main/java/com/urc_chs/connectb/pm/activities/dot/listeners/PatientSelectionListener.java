/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.listeners;

import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;

import org.json.JSONObject;

/**
 * Created by silvap on 1/14/15.
 */
public interface PatientSelectionListener {
    public void OnDotSurveyStarted(JSONObject profile);
    public void OnPatientConfirmOK(SurveyData surveyData);
    public void OnPatientConfirmCancel();
}
