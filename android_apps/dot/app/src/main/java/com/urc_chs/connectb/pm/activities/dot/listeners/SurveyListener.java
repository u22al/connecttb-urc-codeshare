/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.listeners;

import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;

/**
 * Created by silvap on 1/14/15.
 */
public interface SurveyListener {

    public void OnAttentionConfirmed(SurveyData data);

    public void OnConfirmedDosesAdministered(SurveyData data);

    public void OnHasSideEffects(SurveyData data);
    public void OnReportedAllSideEffects(SurveyData data);
    public void OnReportedSideEffectsManagement(SurveyData data);
    public void OnNoSideEffects(SurveyData data);

    public void OnReportedPreviousFacilityVisit(SurveyData data);

    public void OnHasSickAdult(SurveyData data);
    public void OnReportedAdultFacilityVisit(SurveyData data);
    public void OnNoSickAdult(SurveyData data);

    public void OnHasChild(SurveyData data);
    public void OnReportedChildFacilityVisit(SurveyData data);
    public void OnNoChild(SurveyData data);

    public void OnConfirmedFinancialAssistance(SurveyData data);

    public void OnSubmitDOT(SurveyData data);

    public void OnRequestedExit(SurveyData data);
    public void OnRequestedAnotherSurvey(SurveyData data);
}
