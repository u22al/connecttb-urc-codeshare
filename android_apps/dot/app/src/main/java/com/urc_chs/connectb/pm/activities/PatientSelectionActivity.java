/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyDatabase;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyDefnsDatabase;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;
import com.urc_chs.connectb.pm.activities.dot.fragments.PatientConfirmFragment;
import com.urc_chs.connectb.pm.activities.dot.fragments.PatientSelectorFragment;
import com.urc_chs.connectb.pm.activities.dot.listeners.PatientSelectionListener;
import com.urc_chs.connectb.pm.utils.Settings;
import com.urc_chs.connectb.pm.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by silvap on 1/9/15.
 */
public class PatientSelectionActivity
        extends
            ActionBarActivity
        implements
            PatientSelectionListener
{
    private static String TAG = Utils.TAG + "/Main";

    private boolean mPaused = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        savedInstanceState = null;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_connectb);

        initialize(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Force restart activity
        if (mPaused) {
            mPaused = false;

            // http://stackoverflow.com/questions/10844112/runtimeexception-performing-pause-of-activity-that-is-not-resumed
            Handler handler = new Handler();
            handler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    recreate();
                }
            }, 1);
        }
    }

    private void initialize(Bundle savedInstanceState) {

        Settings.update(this);

        if (Settings.getDataValidity() == Utils.DataValidity.DATA_INVALID) {
            // Goto login activity
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return;
        }

        SurveyDatabase.clearErrors();
        SurveyDatabase.update(this);

        SurveyDefnsDatabase.clearErrors();
        SurveyDefnsDatabase.update(this);

        if (SurveyDefnsDatabase.getStatus() != Utils.Status.IDLE) {
            Log.d(TAG, "Attempting to update databases...");
            waitForDatabases(createPostRunner(this, savedInstanceState));
        } else {
            createPostRunner(this, savedInstanceState).run();
        }
    }

    private Runnable createPostRunner(final Context context, final Bundle savedInstanceState) {
        return new Runnable() {
            @Override
            public void run() {
                if (SurveyDefnsDatabase.isAuthError()) {
                    Intent intent = new Intent(PatientSelectionActivity.this, LoginActivity.class);
                    startActivity(intent);
                    return;
                }
                // Check database status' again
                if (SurveyDefnsDatabase.getDataValidity() == Utils.DataValidity.DATA_INVALID) {
                    Log.d(TAG, "Survey Defns data is invalid...");

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("No Data");
                    if (Utils.hasActiveConnection(context)) {
                        builder.setMessage("There seems to be a problem connecting to the server. Try again?");
                        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                waitForDatabases(createPostRunner(context, savedInstanceState));
                            }
                        });
                        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                Intent intent = new Intent(PatientSelectionActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        });
                    } else {
                        builder.setMessage("Not connected to the internet! Try again later.");
                        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                Intent intent = new Intent(PatientSelectionActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        });
                    }
                    builder.setCancelable(false);
                    AlertDialog dialog = builder.create();
                    dialog.show();

                    return;
                }

                try {
                    if (savedInstanceState == null) {
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.container, PatientSelectorFragment.newInstance())
                                .commit();

                        if (SurveyDatabase.getSavedCount() > 0) {
                            if (Utils.hasActiveConnection(PatientSelectionActivity.this)) {
                                SurveyDatabase.uploadSurveys(PatientSelectionActivity.this, new Runnable() {
                                    @Override
                                    public void run() {
                                        // Completed state of surveys may be inconsistent.
                                        // Force refresh to get correct date.
                                        refresh();
                                    }
                                });
                            }
                            else {
                                CharSequence text = "You have " + SurveyDatabase.getSavedCount() + " surveys to be uploaded!";
                                int duration = Toast.LENGTH_LONG;
                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            }
                        }
                    }
                } catch (IllegalStateException e) {
                    // Move on. We will hit this if onSaveInstanceState was called just before.
                    // java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
                    // See: http://www.androiddesignpatterns.com/2013/08/fragment-transaction-commit-state-loss.html
                }
            }
        };
    }

    private void waitForDatabases(final Runnable postRunner) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(this,
                "Connecting to server...",
                "", true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (SurveyDefnsDatabase.getStatus() == Utils.Status.UPDATING) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        break;
                    }
                }
                Log.d(TAG, "Database is done updating..");

                if (postRunner != null) {
                    runOnUiThread(postRunner);
                }

                ringProgressDialog.dismiss();
            }
        }).start();
    }

    @Override
    public void OnDotSurveyStarted(JSONObject profile) {
        SurveyData surveyData = null;
        try {
            surveyData = SurveyData.newInstance(getAppVersion(), profile);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Fragment newFragment = PatientConfirmFragment.newInstance(surveyData);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void OnPatientConfirmOK(SurveyData surveyData) {
        Intent intent = new Intent(this, DotSurveyActivity.class);
        intent.putExtra("surveyData", surveyData.toString());
        startActivity(intent);

        // Close this activity. We don't want it resumed on back button press.
        finish();
    }

    @Override
    public void OnPatientConfirmCancel() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_connectb, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.action_settings:
            {
                // Goto login activity
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.action_upload:
            {
                SurveyDatabase.uploadSurveys(this, null);
                break;
            }
            case R.id.action_refresh:
            {
                refresh();
                break;
            }
            case R.id.action_version:
            {
                CharSequence text = getString(R.string.app_name) + " - Version " + getAppVersion();

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(text);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setCancelable(true);
                AlertDialog dialog = builder.create();
                dialog.show();

                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private String getAppVersion() {
        PackageInfo pkg = null;
        try {
            pkg = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pkg.versionName;
    }

    private void refresh() {
        finish();
        startActivity(getIntent());
    }

}
