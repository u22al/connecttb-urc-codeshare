/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.fragments;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;

import com.urc_chs.connectb.pm.activities.DotSurveyActivity;
import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.adapters.SideEffectsListAdapter;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;
import com.urc_chs.connectb.pm.activities.dot.views.SideEffectsListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ReportSideEffectsFragment extends BaseSurveyFragment {

    public static ReportSideEffectsFragment newInstance(SurveyData surveyData) {
        ReportSideEffectsFragment fragment = new ReportSideEffectsFragment();
        fragment.setSurveyData(surveyData);
        return fragment;
    }

    public ReportSideEffectsFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_dot_report_side_effects, container, false);

        final Button continueBtn = (Button) rootView.findViewById(R.id.continueBtn);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.OnReportedAllSideEffects(mSurveyData);
            }
        });
        continueBtn.setEnabled(false);
        buttonFader.postDelayed(new Runnable() {
            @Override
            public void run() {
                continueBtn.setEnabled(true);
            }
        }, 1000);

        List<JSONObject> sideEffects = new ArrayList<>();
        try {
            JSONArray sideEffectsArray = mSurveyData.getSideEffects();
            for (int i = 0; i < sideEffectsArray.length(); i++) {
                sideEffects.add(sideEffectsArray.getJSONObject(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final SideEffectsListAdapter sideEffectsListAdapter = new SideEffectsListAdapter(getActivity(), sideEffects);

        final SideEffectsListView sideEffectsList = (SideEffectsListView) rootView.findViewById(R.id.sideEffectsList);
        sideEffectsList.setDivider(null);
        sideEffectsList.setAdapter(sideEffectsListAdapter);
        sideEffectsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);

                checkBox.setChecked(!checkBox.isChecked());

                try {
                    DotSurveyActivity activity = (DotSurveyActivity) getActivity();
                    Location location = activity.getLastAcquiredLocation();
                    if (location == null) throw new NullPointerException("Location cannot be null!");

                    mSurveyData.reportSideEffect(position, checkBox.isChecked(), location);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                sideEffectsListAdapter.notifyDataSetChanged();
            }

        });

        return rootView;
    }

}
