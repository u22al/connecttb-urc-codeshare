/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyDefnsDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by silvap on 1/9/15.
 */
public class ExpandingPatientsListAdapter extends BaseExpandableListAdapter implements Filterable {

    JSONArray mAllProfiles;
    JSONArray mFilteredProfiles;
    Activity mContext;
    Filter mFilter = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            int count = mAllProfiles.length();
            final JSONArray filteredList = new JSONArray();

            for (int i = 0; i < count; i++) {
                try {
                    JSONObject surveyDefn = mAllProfiles.getJSONObject(i);
                    JSONObject patient = surveyDefn.getJSONObject("patient");
                    String targetString = patient.getString("name");
                    if (targetString.toLowerCase().contains(filterString)) {
                        filteredList.put(surveyDefn);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            results.values = filteredList;
            results.count = filteredList.length();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFilteredProfiles = (JSONArray) results.values;
            notifyDataSetChanged();
        }
    };
    OnPatientVisitListener mPatientVisitListener;

    public interface OnPatientVisitListener {
        public void onPatientVisit(JSONObject profile);
    }

    public ExpandingPatientsListAdapter(Activity context,
                                        JSONArray surveyDefns,
                                        OnPatientVisitListener listener) {
        super();

        mAllProfiles = new JSONArray();
        // Only show status == 'Active' patients
        int count = surveyDefns.length();
        for (int i = 0; i < count; i++) {
            try {
                JSONObject surveyDefn = surveyDefns.getJSONObject(i);
                JSONObject patient = surveyDefn.getJSONObject("patient");
                String status = patient.getString("status");
                if (status.equals("Active")) {
                    mAllProfiles.put(surveyDefn);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        mFilteredProfiles = mAllProfiles;
        mContext = context;
        mPatientVisitListener = listener;
    }

    @Override
    public int getGroupCount() {
        return mFilteredProfiles.length();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        try {
            return mFilteredProfiles.getJSONObject(groupPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        try {
            return mFilteredProfiles.getJSONObject(groupPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        JSONObject patient = null;
        try {
            patient = ((JSONObject) getGroup(groupPosition)).getJSONObject("patient");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        View vi = convertView;

        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.view_patient_listitem, parent, false);
        }

        TextView name = (TextView) vi.findViewById(R.id.drug);
        try {
            name.setText(patient.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            if (SurveyDefnsDatabase.hasCompletedToday(
                    ((JSONObject) getGroup(groupPosition)).getString("_id"))
               ) {
                name.setPaintFlags(name.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                name.setPaintFlags(name.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //if (isExpanded) vi.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY);
        //else            vi.getBackground().clearColorFilter();

        return vi;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        JSONObject patient = null;
        try {
            patient = ((JSONObject) getGroup(groupPosition)).getJSONObject("patient");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        View vi = convertView;

        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.view_patient_listitem_child, parent, false);
        }

        TextView lastVisit = (TextView) vi.findViewById(R.id.lastVisit);
//        String lastVisitMsg = getContext().getResources().getString(R.string.never);
//        if (patient.getLastVisit() != null) {
//            lastVisitMsg = DateFormat.getDateTimeInstance().format(patient.getLastVisit());
//        }
//        lastVisit.setText(
//                getContext().getResources().getString(R.string.last_visit, lastVisitMsg)
//        );

//        String statusMsg = null;
//        try {
//            statusMsg = "Status: " + patient.getString("status");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        lastVisit.setText(statusMsg);

        Button visitBtn = (Button) vi.findViewById(R.id.visitBtn);
        visitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject definition = null;
                try {
                    definition = mFilteredProfiles.getJSONObject(groupPosition);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mPatientVisitListener.onPatientVisit(definition);
            }
        });
        try {
            if (SurveyDefnsDatabase.hasCompletedToday(
                    ((JSONObject) getGroup(groupPosition)).getString("_id"))
                    ) {
                visitBtn.setEnabled(false);
                visitBtn.setPaintFlags(visitBtn.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                lastVisit.setText("Completed for Today");
            } else {
                visitBtn.setEnabled(true);
                visitBtn.setPaintFlags(visitBtn.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                lastVisit.setText(null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return vi;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }
}
