/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.login.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.urc_chs.connectb.pm.R;

/**
 * Created by silvap on 4/4/15.
 */
public class PrivacyConfirmFragment extends BaseLoginFragment {

    public static PrivacyConfirmFragment newInstance() {
        PrivacyConfirmFragment fragment = new PrivacyConfirmFragment();
        return fragment;
    }

    public PrivacyConfirmFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_privacy_confirm, container, false);

        final Button continueButton = (Button) rootView.findViewById(R.id.continueBtn);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.OnPrivacyConfirm();
            }
        });
        continueButton.setEnabled(false);

        buttonFader.postDelayed(new Runnable() {
            @Override
            public void run() {
                continueButton.setEnabled(true);
            }
        }, 1000);

        return rootView;
    }
}
