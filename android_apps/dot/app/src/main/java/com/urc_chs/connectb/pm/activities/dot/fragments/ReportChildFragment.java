/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.fragments;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.urc_chs.connectb.pm.activities.DotSurveyActivity;
import com.urc_chs.connectb.pm.R;
import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;

import org.json.JSONException;

public class ReportChildFragment extends BaseSurveyFragment {

    public static ReportChildFragment newInstance(SurveyData surveyData) {
        ReportChildFragment fragment = new ReportChildFragment();
        fragment.setSurveyData(surveyData);
        return fragment;
    }

    public ReportChildFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_dot_report_child, container, false);

        final RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.radioGroup);

        final Button button = (Button) rootView.findViewById(R.id.continueBtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean visited = (radioGroup.getCheckedRadioButtonId() == R.id.visited);
                try {
                    DotSurveyActivity activity = (DotSurveyActivity) getActivity();
                    Location location = activity.getLastAcquiredLocation();
                    if (location == null) throw new NullPointerException("Location cannot be null!");

                    mSurveyData.setChildBroughtToFacility(visited, location);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mListener.OnReportedChildFacilityVisit(mSurveyData);
            }
        });
        button.setEnabled(false);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                button.setEnabled(true);
            }
        });

        return rootView;
    }
}
