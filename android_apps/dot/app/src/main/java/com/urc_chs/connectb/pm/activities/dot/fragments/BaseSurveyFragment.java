/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;

import com.urc_chs.connectb.pm.activities.dot.data.SurveyData;
import com.urc_chs.connectb.pm.activities.dot.listeners.SurveyListener;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Created by silvap on 1/11/15.
 */
public abstract class BaseSurveyFragment extends Fragment {

    private static final String ARG_SURVEY_DATA = "surveyData";

    protected SurveyData mSurveyData;
    protected SurveyListener mListener = null;

    protected Handler buttonFader = new Handler();

    public BaseSurveyFragment() {
        super();
    }

    public void setSurveyData(SurveyData surveyData) {
        Bundle args = new Bundle();
        args.putString(ARG_SURVEY_DATA, surveyData.toString());
        setArguments(args);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            try {
                String json = getArguments().getString(ARG_SURVEY_DATA);
                JSONObject data = (JSONObject) new JSONTokener(json).nextValue();
                mSurveyData = new SurveyData(true, data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SurveyListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement SurveyListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
