/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.md', which is part of this source code package.
 */

package com.urc_chs.connectb.pm.activities.dot.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.urc_chs.connectb.pm.utils.Settings;
import com.urc_chs.connectb.pm.utils.TimestampUtils;
import com.urc_chs.connectb.pm.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.urc_chs.connectb.pm.utils.Utils.Status;
import static com.urc_chs.connectb.pm.utils.Utils.DataValidity;

/**
 * Created by silvap on 1/17/15.
 */
public class SurveyDefnsDatabase {

    public static String TAG = Utils.TAG + "/SurveyDefns";

    private static String PREFS_KEY = "SurveyDefns";
    private static String SIDE_EFFECTS = "sideEffects";
    private static String PROFILES = "profiles";
    private static String FINISHED_SURVEYS_TODAY = "finishedSurveysToday";
    private static String TEST_MODE = "testMode";

    private static SurveyDefnsDatabase ourInstance = new SurveyDefnsDatabase();

    public static SurveyDefnsDatabase getInstance() {
        return ourInstance;
    }

    private volatile JSONArray mProfiles = new JSONArray();
    private volatile JSONArray mSideEffects = new JSONArray();
    private volatile JSONArray mFinishedSurveysToday = new JSONArray();
    private volatile boolean mTestMode = false;

    private volatile Status mStatus = Status.IDLE;
    private volatile DataValidity mValidity = DataValidity.DATA_INVALID;

    private volatile boolean mAuthError = false;

    private SurveyDefnsDatabase() {

    }

    private void loadFromStorage(SharedPreferences preferences) {
        Log.d(TAG, "Loading from storage...");
        // SideEffects
        String json = preferences.getString(SIDE_EFFECTS, "");
        try {
            mSideEffects = new JSONArray(json);
        } catch (JSONException e) {
            Log.d(TAG, "Load from storage failed");
            mValidity = DataValidity.DATA_INVALID;
            return;
        }
        // Profiles
        json = preferences.getString(PROFILES, "");
        try {
            mProfiles = new JSONArray(json);
        } catch (JSONException e) {
            Log.d(TAG, "Load from storage failed");
            mValidity = DataValidity.DATA_INVALID;
            return;
        }
        // Finished surveys today
        json = preferences.getString(FINISHED_SURVEYS_TODAY, "");
        try {
            mFinishedSurveysToday = new JSONArray(json);
        } catch (JSONException e) {
            Log.d(TAG, "Load from storage failed");
            mValidity = DataValidity.DATA_INVALID;
            return;
        }
        // Test mode
        mTestMode = preferences.getBoolean(TEST_MODE, false);

        mValidity = mSideEffects.length() != 0 ? DataValidity.DATA_VALID : DataValidity.DATA_INVALID;
        Log.d(TAG, "Loaded: SideEffects(" + mSideEffects.length() + ") " +
                            "Profiles(" + mProfiles.length() + ") " +
                            "Finished Surveys(" + mFinishedSurveysToday.length() + ")" +
                            "Test Mode(" + (mTestMode ? "true" : "false") + ")");
    }

    private void saveToStorage(SharedPreferences preferences) {
        Log.d(TAG, "Saving to storage...");
        SharedPreferences.Editor editor = preferences.edit();
        // SideEffects
        editor.putString(SIDE_EFFECTS, mSideEffects.toString());
        // Profiles
        editor.putString(PROFILES, mProfiles.toString());
        // Finished surveys
        editor.putString(FINISHED_SURVEYS_TODAY, mFinishedSurveysToday.toString());
        // Test mode
        editor.putBoolean(TEST_MODE, mTestMode);
        //
        editor.commit();
        Log.d(TAG, "Saving completed");
    }

    private void refresh(Context ctx) {

        if (!Utils.hasActiveConnection(ctx)) {
            Log.d(TAG, "No connection. Skipping refresh");
            return;
        }

        Log.d(TAG, "Refreshing...");
        mStatus = Status.UPDATING;
        new GetSurveyDefnsDataTask(ctx).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public synchronized static boolean isAuthError() {
        return getInstance().mAuthError;
    }
    public synchronized static void setAuthError() {
        getInstance().mAuthError = true;
    }
    public synchronized static void clearErrors() {
        getInstance().mAuthError = false;
    }

    public synchronized static Status getStatus() {
        return ourInstance.mStatus;
    }

    public synchronized static DataValidity getDataValidity() {
        return ourInstance.mValidity;
    }

    public synchronized static JSONArray getAllSideEffects() {
        return getInstance().mSideEffects;
    }

    public synchronized static JSONArray getAllProfiles() {
        return getInstance().mProfiles;
    }

    public synchronized static JSONArray getAllFinishedSurveysToday() { return getInstance().mFinishedSurveysToday; }

    public synchronized static boolean isTestMode() { return getInstance().mTestMode; }

    public static boolean hasCompletedToday(String profileId) {
        if (isTestMode()) return false;

        // Check finished surveys
        JSONArray finishedSurveys = getAllFinishedSurveysToday();
        int count = finishedSurveys.length();
        try {
            Date startOfToday = TimestampUtils.startOfToday();
            for (int i = 0; i < count; i++) {
                JSONObject survey = finishedSurveys.getJSONObject(i);
                JSONObject data = survey.getJSONObject("data");

                JSONObject profile = data.getJSONObject("surveyProfile");
                String checkId = profile.getString("_id");
                if (profileId.equals(checkId)) {

                    // Check if survey is still valid for today
                    Date completionDate = TimestampUtils.getDateForISO8601String(
                            data.getJSONObject("completedTime").getString("time")
                    );
                    if (completionDate.after(startOfToday)) {
                        return true;
                    }
                }
            }
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }

        // Check survey database
        JSONArray pendingSurveys = SurveyDatabase.getPendingSurveys();
        count = pendingSurveys.length();
        try {
            Date startOfToday = TimestampUtils.startOfToday();
            for (int i = 0; i < count; i++) {
                JSONObject survey = pendingSurveys.getJSONObject(i);
                JSONObject data = survey; //survey.getJSONObject("data");

                JSONObject profile = data.getJSONObject("surveyProfile");
                String checkId = profile.getString("_id");
                if (profileId.equals(checkId)) {

                    // Check if survey is still valid for today
                    Date completionDate = TimestampUtils.getDateForISO8601String(
                            data.getJSONObject("completedTime").getString("time")
                    );
                    if (completionDate.after(startOfToday)) {
                        return true;
                    }
                }
            }
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    public synchronized static void update(Context ctx) {
        Log.d(TAG, "Updating...");
        if (ourInstance.mValidity == DataValidity.DATA_INVALID) {
            ourInstance.loadFromStorage(ctx.getSharedPreferences(PREFS_KEY, 0));
        }

        ourInstance.refresh(ctx);
    }

    public synchronized static void setData(Context ctx, JSONObject data) {
        Log.d(TAG, "Start set data..");
        if (data == null) {
            ourInstance.mStatus = Status.IDLE;
            throw new NullPointerException("Data is null");
        }

        JSONArray sideEffects = null;
        try {
            sideEffects = data.getJSONArray("sideEffects");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONArray profiles = null;
        try {
            profiles = data.getJSONArray("profiles");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONArray finishedSurveysToday = null;
        try {
            finishedSurveysToday = data.getJSONArray("finishedSurveysToday");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        boolean testMode = false;
        try {
            testMode = data.getBoolean("testMode");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (sideEffects == null || profiles == null || finishedSurveysToday == null) {
            ourInstance.mStatus = Status.IDLE;
            Log.d(TAG, "Data seems to be invalid..");
            return;
        }

        Log.d(TAG, "Setting new data: SideEffects(" + sideEffects.length() + ") " +
                                      "Profiles(" + profiles.length() + ") " +
                                      "Finished Surveys Today(" + finishedSurveysToday.length() + ") " +
                                      "Test Mode(" + (testMode ? "true" : "false") + ")");
        ourInstance.mSideEffects = sideEffects;
        ourInstance.mProfiles = profiles;
        ourInstance.mFinishedSurveysToday = finishedSurveysToday;
        ourInstance.mTestMode = testMode;

        ourInstance.saveToStorage(ctx.getSharedPreferences(PREFS_KEY, 0));

        ourInstance.mValidity = DataValidity.DATA_VALID;
        ourInstance.mStatus = Status.IDLE;
    }
}

class GetSurveyDefnsDataTask extends AsyncTask<Void, Void, JSONObject> {

    private final Context mContext;
    private Exception exception = null;

    public GetSurveyDefnsDataTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected JSONObject doInBackground(Void... params){

        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;
        try {
            String url = Settings.getServerUrl() + "/dot/provider-surveys";
            List<NameValuePair> getParams = new ArrayList<>();

            getParams.add( new BasicNameValuePair( "login", Settings.getUsername() ) );
            getParams.add( new BasicNameValuePair( "token", Settings.getToken() ) );

            url = url + "?" + URLEncodedUtils.format(getParams, "utf-8");
            //
            HttpGet getRequest = new HttpGet(url);
            response = httpclient.execute(getRequest);
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();
            }
            else if (statusLine.getStatusCode() == HttpStatus.SC_FORBIDDEN) {
                //Closes the connection.
                response.getEntity().getContent().close();
                Log.d(SurveyDatabase.TAG, "Cannot retrieve profiles! " + statusLine.getReasonPhrase());
                //
                SurveyDefnsDatabase.setAuthError();
                //
                return new JSONObject();
            }
            else {
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            Log.d(SurveyDefnsDatabase.TAG, "Encountered ClientProtocolException!" + e.getMessage());
            return new JSONObject();
        } catch (IOException e) {
            Log.d(SurveyDefnsDatabase.TAG, "Encountered IOException! " + e.getMessage());
            return new JSONObject();
        }

        JSONObject ret = null;
        try {
            ret = new JSONObject(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ret;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        SurveyDefnsDatabase.setData(mContext, jsonObject);
    }
}