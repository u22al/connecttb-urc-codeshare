Copyright (c) University Research Co., LLC (URC) - All Rights Reserved.
Unauthorized copying of the source code, via any medium is strictly prohibited.
Proprietary and confidential.

Developed by Prasad Silva (Bitsemble, LLC), 2015